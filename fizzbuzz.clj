(ns fizzbuzz)

(defn fizzbuzz
  "Take number, and see if it's divisible by 3 and/or 5"
  [^long number]
  
  (cond
    (= 0 (mod number 15)) (println "FizzBuzz")
    (= 0 (mod number 3)) (println "Fizz")
    (= 0 (mod number 5)) (println "Buzz")
    :else (println number))

  (if (< number 100)
    (fizzbuzz (+ number 1))))

(defn run
  "Run the program"
  [opts]
  (fizzbuzz 0))
